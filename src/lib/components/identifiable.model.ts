import { guid } from "./guid.model";

export interface Identifiable {
    readonly guid: guid;
    readonly dateCreated: Date;
    name: string;
    description: string;
}