export interface Editable {
    dateUpdated: Date;
}