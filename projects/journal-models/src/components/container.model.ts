import { guid } from "./guid.model";

export interface Container {
    members: guid[];
}