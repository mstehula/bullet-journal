import * as uuid from 'uuid';

//uuid uses string under the hood, typedef to give it a bit more uniqueness
export type guid = string;

export namespace GUID {
    //Creates a guid using uuid.v4()
    export function createGuid(): guid {
        return uuid.v4();
    }

    //Creates a guid which is all 0s
    export function createEmptyGuid(): guid {
        return uuid.NIL;
    }

    //Validates that we are using a properly formed guid
    export function validate(guid: guid): boolean {
        return uuid.validate(guid);
    }
}

