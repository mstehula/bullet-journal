import { Container } from "./components/container.model";
import { Editable } from "./components/editable.model";
import { Identifiable } from "./components/identifiable.model";

export interface Library extends Identifiable, Editable, Container {};