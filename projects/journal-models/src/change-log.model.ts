import { Identifiable } from "./components/identifiable.model";

export interface ChangeLogEntry extends Identifiable {
    //This index is the number in the change log. Index should remain in order, index 0 should be at start of array.
    //This is to check if the entries are in order. Should have a very detailed list at the end of when things were changed
    index: number;

    //These are the items that were changed. We're not going to be running the commands again, simply applying the pre and post
    //Undo -> Apply pre
    //Redo -> Apply post
    pre: Identifiable;
    post: Identifiable;
}

export interface ChangeLog extends Identifiable {
    //Entries of change log. All change log entries should be in index order.
    entries: ChangeLogEntry[];
}